# Overview

MarshallZehr employees deal with financial information all day every day.  Sometimes, that information is denominated in
currencies other than Canadian dollars.  We convert foreign currencies to Canadian dollars on the date of the
transaction, allowing us to compare any combination of transactions in our system.

You will build a small application to demonstrate your coding skills that converts an amount in a foreign currency into
an amount in Canadian currency on a specific date.

## Requirements

Your application should meet the following requirements:

1.	Convert between foreign currencies and Canadian dollars, and from Canadian dollars to foreign currencies
2.  Use the daily average exchange rate published by the Bank of Canada for conversion between foreign currencies and
    Canadian dollars
3.  Permit the user to specify the amount of currency to convert
4.	Permit the user to specify the foreign currency to convert to/from by currency code (ISO 4217)
5.	Permit the user to optionally specify the date of the conversion (for converting historical values)
6.	Display the output value, the conversion rate used, and the date of conversion
7.	Display the output value to a precision of 4 decimal places
8.	Convert at least the following foreign currencies: USD, EUR, JPY, GBP, AUD, CHF, CNY, HKD, MXN, INR

## Format/Framework

If you are applying for a **front-end position**, please build a small **web-application in Angular**.

If you are applying for a **back-end position**, please build a small **console application in .NET Core**.

If you are applying as a full-stack or other developer, you may choose either format.

Please submit only a single application.

## Evaluation Criteria

Our overall goal with this coding check is to determine two things:

1.	Are you able to turn a business requirement into working code?
2.  Do you code in a manner that is compatible with our team's engineering principles to create scalable, maintainable,
    reliable, and high-quality software for our end users?

The first one will be evaluated by building and running your application and using it to do some currency conversions.
We compare your output with a list of known amounts in a few currencies on specific dates (including the evaluation
date).

The second one will be evaluated by looking at a range of factors and seeing how your application demonstrates those
factors.  We will not be sharing our full checklist, but to give you an idea:

* How long did it take us to run your application for the first time? Were there any problems we needed to fix before we
  got it running?
* Is the application well-structured, and does it follow modern best-practices for the framework?
* Is the application clear and easy-to-use for the end user?
* Would it be easy to maintain/extend the application in the future?
* Is the developer experience good?
* What sort of documentation is provided, and for whom?  Is the format appropriate?
* What other development practices are demonstrated?

There is no "right answer" for this section – there are many good ways to develop software.  We are simply looking to
understand your perspective on how to build high-quality products – setting us up for some great discussions at your
in-person interview.

## Not Evaluated

We will not be considering the following in our evaluation, so do not feel a need to put any effort into any of these
items:

* Graphics design (how "pretty" the application is)
* Deployment to end-user machines (we will build from source and run locally)
* Versioning the application (outside of using a repository for source code)
* The time it takes to submit your application 
* How you arrived at your final submission

## Submission

To submit your application, please send us a link to a GitHub/GitLab/Bitbucket repository.

## Time

Budget an hour or two to build the application; we are not looking for a masterpiece.

You can take as long as you need to submit – we understand people have jobs and lives, so do what fits with your
schedule. 

## Next Steps

After you submit, we will review your application with a target to get next steps out to you within 2 business days.  If
you do not hear from us by then, feel free to follow up.

The next step in our interview process is an in-person (or video) interview.

## Questions

In general, if you are unsure about an element of this coding check you should state your assumption and proceed.  We
will consider any notes you send during our evaluation.

If you believe you cannot proceed without an answer, you can email us and we will update this README to clarify

Note that we are intentionally giving high-level guidance rather than detailed specification to allow you a lot of
freedom in your implementation.

## Copyright

© MarshallZehr Group Inc.  All Rights Reserved.
